export class SocketNotInitializedError extends Error {
    constructor() {
        super('SocketNotInitialized');
    }
}