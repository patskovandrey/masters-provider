import axios from 'axios';
import type {AxiosResponse, AxiosRequestConfig} from 'axios';
import {io, Socket} from 'socket.io-client';
import {SocketNotInitializedError} from './errors';

interface ProviderConfig {
  serviceUrl: string
  socketUrl: string
}

type UploadFileResult = {
  id: string,
  poster?: string,
}

export default class Provider {
  readonly serviceUrl: string
  readonly socketUrl: string
  private socket_: Socket | null = null
  private queue_: [string, any][] = []

  constructor(config: ProviderConfig) {
    this.serviceUrl = config.serviceUrl;
    this.socketUrl = config.socketUrl;
  }

  connect() {
    this.socket_ = io(this.socketUrl, {
      closeOnBeforeunload: false,
    });
    this.socket_.on('connect', () => {
      this.queue_.forEach(([event, data]) => {
        this.socket_?.emit(event, data);
      });
      this.queue_ = [];
    });
  }

  get<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    return this.request({
      url,
      method: 'get',
      ...config,
    })
  }

  post<T = any>(url: string, data: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    return this.request({
      url,
      method: 'post',
      data,
      ...config,
    });
  }

  request<T = any>(config: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const headers = {...config.headers || {}};
    if (this.socket_) {
      headers['X-Socket-Id'] = this.socket_.id;
    }
    return axios.request<T>({
      ...config,
      url: this.joinServiceUrl(config.url || ''),
      headers,
    });
  }

  async uploadFile(file: File, onUploadProgress?: (progressEvent: ProgressEvent) => void): Promise<UploadFileResult> {
    const formData = new FormData();
    formData.append('file', file);
    const result = await this.post<UploadFileResult>('/upload', formData, {
      onUploadProgress
    });
    return result.data;
  }

  emit<T>(event: string, data: T): void {
    if (!this.socket_) {
      throw new SocketNotInitializedError()
    }
    if (!this.socket_.connected) {
      this.queue_.push([event, data]);
    } else {
      this.socket_.emit(event, data);
    }
  }

  on(event: string, listener: (...args: any[]) => void): void {
    if (!this.socket_) {
      throw new SocketNotInitializedError()
    }

    this.socket_.on(event, listener);
  }

  off(event: string, listener: (...args: any[]) => void): void {
    if (!this.socket_) {
      throw new SocketNotInitializedError()
    }

    this.socket_.off(event, listener);
  }

  private joinServiceUrl(url: string): string {
    return this.serviceUrl + url;
  }

  getDownloadLink (fileId: string): string {
    return this.joinServiceUrl(`/file/${fileId}`);
  }
}
