"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var socket_io_client_1 = require("socket.io-client");
var errors_1 = require("./errors");
var Provider = /** @class */ (function () {
    function Provider(config) {
        this.socket_ = null;
        this.queue_ = [];
        this.serviceUrl = config.serviceUrl;
        this.socketUrl = config.socketUrl;
    }
    Provider.prototype.connect = function () {
        var _this = this;
        this.socket_ = (0, socket_io_client_1.io)(this.socketUrl, {
            closeOnBeforeunload: false,
        });
        this.socket_.on('connect', function () {
            _this.queue_.forEach(function (_a) {
                var _b;
                var event = _a[0], data = _a[1];
                (_b = _this.socket_) === null || _b === void 0 ? void 0 : _b.emit(event, data);
            });
            _this.queue_ = [];
        });
    };
    Provider.prototype.get = function (url, config) {
        return this.request(__assign({ url: url, method: 'get' }, config));
    };
    Provider.prototype.post = function (url, data, config) {
        return this.request(__assign({ url: url, method: 'post', data: data }, config));
    };
    Provider.prototype.request = function (config) {
        var headers = __assign({}, config.headers || {});
        if (this.socket_) {
            headers['X-Socket-Id'] = this.socket_.id;
        }
        return axios_1.default.request(__assign(__assign({}, config), { url: this.joinServiceUrl(config.url || ''), headers: headers }));
    };
    Provider.prototype.uploadFile = function (file, onUploadProgress) {
        return __awaiter(this, void 0, void 0, function () {
            var formData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        formData = new FormData();
                        formData.append('file', file);
                        return [4 /*yield*/, this.post('/upload', formData, {
                                onUploadProgress: onUploadProgress
                            })];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result.data];
                }
            });
        });
    };
    Provider.prototype.emit = function (event, data) {
        if (!this.socket_) {
            throw new errors_1.SocketNotInitializedError();
        }
        if (!this.socket_.connected) {
            this.queue_.push([event, data]);
        }
        else {
            this.socket_.emit(event, data);
        }
    };
    Provider.prototype.on = function (event, listener) {
        if (!this.socket_) {
            throw new errors_1.SocketNotInitializedError();
        }
        this.socket_.on(event, listener);
    };
    Provider.prototype.off = function (event, listener) {
        if (!this.socket_) {
            throw new errors_1.SocketNotInitializedError();
        }
        this.socket_.off(event, listener);
    };
    Provider.prototype.joinServiceUrl = function (url) {
        return this.serviceUrl + url;
    };
    Provider.prototype.getDownloadLink = function (fileId) {
        return this.joinServiceUrl("/file/".concat(fileId));
    };
    return Provider;
}());
exports.default = Provider;
//# sourceMappingURL=provider.js.map