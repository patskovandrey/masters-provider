import type { AxiosResponse, AxiosRequestConfig } from 'axios';
interface ProviderConfig {
    serviceUrl: string;
    socketUrl: string;
}
declare type UploadFileResult = {
    id: string;
    poster?: string;
};
export default class Provider {
    readonly serviceUrl: string;
    readonly socketUrl: string;
    private socket_;
    private queue_;
    constructor(config: ProviderConfig);
    connect(): void;
    get<T = any>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>>;
    post<T = any>(url: string, data: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>>;
    request<T = any>(config: AxiosRequestConfig): Promise<AxiosResponse<T>>;
    uploadFile(file: File, onUploadProgress?: (progressEvent: ProgressEvent) => void): Promise<UploadFileResult>;
    emit<T>(event: string, data: T): void;
    on(event: string, listener: (...args: any[]) => void): void;
    off(event: string, listener: (...args: any[]) => void): void;
    private joinServiceUrl;
    getDownloadLink(fileId: string): string;
}
export {};
